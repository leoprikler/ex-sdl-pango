CC=gcc
CFLAGS=`pkg-config --cflags pangocairo` `pkg-config --cflags sdl2`
LIBS=`pkg-config --libs pangocairo` `pkg-config --libs sdl2`

all: ex-sdl-pango

ex-sdl-pango: ex-sdl-pango.c
	$(CC) $< $(CFLAGS) -o $@ $(LDFLAGS) $(LIBS)

clean:
	rm -f ex-sdl-cairo-freetype-pango
